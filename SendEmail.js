import nodemailer from 'nodemailer';

const Email = (options) =>{
    let transporter = nodemailer.createTransport({
        service: 'gmail', //depende del correo que utilicemos
        auth:{
            user: process.env.USER, 
            pass: process.env.PASSWORD
        }
    })
    transporter.sendMail(options, (err,info) =>{
        if (err) {
            console.log(err);
            return;
        }
    });
};

// Send email

const EmailSender = ({name,email,message}) =>{
    const options = {
        from: `Jesus Marcano <${process.env.USER}>`,
        to:process.env.SEND_TO,
        subject: "Message from jesus marcano",
        html:`
        <div style="width: 100%; background-color: #f3f9ff; padding: 5rem 0">
        <div style="max-width: 700px; background-color: white; margin: 0 auto; border: 2px solid black; border-radius: 20px">
          <div style="width: 100%; background-color: #00efbc; padding: 20px 0; border-bottom: 2px solid black; border-top-left-radius: 20px; border-top-right-radius: 20px;">
          <a href="${process.env.CLIENT_URL}" ><img
              src= "https://cdn.icon-icons.com/icons2/2351/PNG/512/logo_gmail_envelope_letter_email_icon_143171.png"
              style="width: 100%; height: 70px; object-fit: contain"
            /></a> 
          
          </div>
          <div style="width: 100%; gap: 10px; padding: 30px 0; display: grid">
            <p style="font-weight: 800; font-size: 1.2rem; padding: 0 30px">
              Alguien está interesado en tu perfil
            </p>
            <div style="font-size: 15px; margin: 0 30px">
              <p>Nombre: <b>${name}</b></p>
              <p>Email: <b>${email}</b></p>
              <p>Mensaje: <i>${message}</i></p>
            </div>
          </div>
        </div>
      </div>
        `,
    };

      Email(options)
};

export default EmailSender